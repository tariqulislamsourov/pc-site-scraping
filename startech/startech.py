import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient
import re
import urllib3
from bs4.dammit import EncodingDetector
from urllib.error import HTTPError
from urllib.error import URLError


#----------------------------------------------------------------------
# Give an URL and get the HTML from that url
#----------------------------------------------------------------------
headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'}
def getHtmlFromURL(url):
    response = requests.get(url, headers=headers)
    http_encoding = response.encoding if 'charset' in response.headers.get('content-type', '').lower() else None
    html_encoding = EncodingDetector.find_declared_encoding(response.content, is_html=True)
    encoding = html_encoding or http_encoding
    outputhtml = BeautifulSoup(response.content, 'lxml', from_encoding=encoding)
    return outputhtml

#----------------------------------------------------------------------
#Remove Duplication from the list (Sometimes list have duplicate url)
#----------------------------------------------------------------------
def findDuplicateInList(listName):
    b = set()
    unique = []

    for x in listName:
        if x not in b:
            unique.append(x)
            b.add(x)
    return unique

def get_product_details_url(url):
    product_details_url_list = []
    active_url = url
    product_list_page_soup = getHtmlFromURL(active_url)
    find_product_list_block = product_list_page_soup.find('div', {"class": "main-content p-items-wrap"})
    find_product_list = find_product_list_block.find_all('h4',{"class": "p-item-name"})
    for each_product in find_product_list:
        find_each_product_link = each_product.find('a')
        each_product_link = find_each_product_link.get("href")
        get_product_details(each_product_link)
        product_details_url_list.append(each_product_link)
    # print(product_details_url_list)

def get_product_details(url):
    active_link = url
    short_description_list = []
    short_info_list = []
    product_specification_list = []
    product_detail_soup = getHtmlFromURL(active_link)
    # print(product_detail_soup)
    find_product_short_info = product_detail_soup.find('div', {"class" : "product-short-info"})
    find_product_short_description = product_detail_soup.find('div', {"class" : "short-description"})
    product_name = find_product_short_info.find('h1', {"class": "product-name"}).text
    # print(product_name)

    product_short_info_table = find_product_short_info.find('table', {"class": "product-info-table"})
    product_short_info_table_data_group = product_short_info_table.find_all("tr", {"class": "product-info-group"})
    for each_column in product_short_info_table_data_group:
        product_short_info_table_data_column_name = each_column.find('td', {"class": "product-info-label"}).text
        product_short_info_table_data_column_value = each_column.find('td', {"class": "product-info-data"}).text
        short_info_list.append({
            'info_lable': product_short_info_table_data_column_name,
            'info_value': product_short_info_table_data_column_value
        })
    product_short_description = find_product_short_description.find_all('li')
    for each_descriptons in product_short_description:
        if 'view-more' not in str(each_descriptons):
            short_decription_text = each_descriptons.text
            short_description_list.append(short_decription_text)

    # print(short_description_list)
    # print(short_info_list)

    find_product_specification_block = product_detail_soup.find('section', {"class": "specification-tab"})
    find_product_specification_table = find_product_specification_block.find('table', {"class": "data-table"})
    find_product_specification_table_body = find_product_specification_table.find('tbody')
    find_product_specification_table_body_tr = find_product_specification_table_body.find_all('tr')
    for each_data_row in find_product_specification_table_body_tr:
        find_product_specification_data_column_name = each_data_row.find('td', {"class": "name"}).text
        find_product_specification_data_column_value = each_data_row.find('td', {"class": "value"}).text
        product_specification_list.append({
            'info_lable': find_product_specification_data_column_name,
            'info_value': find_product_specification_data_column_value
        })

    print('name:')
    print(product_name)
    print('short info:')
    print(short_info_list)
    print('short description:')
    print(short_description_list)
    print('specification:')
    print(product_specification_list)

    save_data_into_db(product_name, short_info_list, short_description_list, product_specification_list)

def save_data_into_db(name, short_info, short_description, specification):
    db.startech_product_details.update_one(
                        {"Name": name},
                        {"$set":
                            {
                            "short_info": short_info,
                            "short_description": short_description,
                            "specification": specification,
                            }
                        }, upsert=True)


try:
    # Connect with the portnumber and hostheaders
    client = MongoClient("mongodb://localhost:27017/")
    print("MongoDB Connected successfully!!!")
except:
    print("Could not connect to MongoDB")

db = client['startech_bd']
companyinfo = db['filtered_8947']

company_base_url = 'https://www.startech.com.bd/'
url = "https://www.startech.com.bd/"
number = 1
active_link = url

each_product_type_url_list = []
while True:
    
    print(active_link)
    soup1 = getHtmlFromURL(active_link)
    if soup1 is None:
        continue
    else:
        find_product_category_listing = soup1.find_all('li', {"class" : "nav-item"})
        for product_category_listing in find_product_category_listing:
            if "has-child" in str(product_category_listing):
                # print('has child')
                continue
            else:
                find_product_category_url = product_category_listing.find('a', {"class": "nav-link"})
                product_category_url = find_product_category_url.get('href')
                each_product_type_url_list.append(str(product_category_url))
    # print(each_product_type_url_list)  

    for each_product_type_url in each_product_type_url_list:
        each_product_type_url_soup = getHtmlFromURL(each_product_type_url)
        # print(each_product_type_url_soup)
        find_current_page_of_this_type = each_product_type_url_soup.find('ul', {"class": "pagination"})
        if find_current_page_of_this_type:
            pagination_link_list = []
            find_pagination_links = find_current_page_of_this_type.find_all('a')
            for find_pagination_link in find_pagination_links:
                if 'NEXT' in str(find_pagination_link):
                    continue
                else:
                    pagination_link = find_pagination_link.get("href")
                    pagination_link_list.append(pagination_link)
            print(each_product_type_url)
            print(pagination_link_list)
        else:
            print(each_product_type_url)
            print("no pagination")
            get_product_details_url(each_product_type_url)



       